import * as dat from 'dat.gui'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'
import fireFliesVertexShader from '/shaders/fireflies/vertex.glsl'
import fireFliesFragmentShader from '/shaders/fireflies/fragment.glsl'
import portalVertexShader from '/shaders/portal/vertex.glsl'
import portalFragmentShader from '/shaders/portal/fragment.glsl'
import { gsap } from "gsap";
import Splitting from "splitting";

Splitting();

const loaderImage = document.querySelector('.loader')
const content = document.querySelector('.content')

const fx17Titles = [...document.querySelectorAll('.content__title[data-splitting][data-effect17]')];

const scroll = () => {

  content.style.opacity = 1
  
  fx17Titles.forEach(title => {
        
    const chars = title.querySelectorAll('.char');
    
      chars.forEach(char => gsap.set(char.parentNode, { perspective: 1000 })); 
  
      gsap.fromTo(chars, { 
        'will-change': 'opacity, transform', 
        opacity: 0,
        rotateX: () => gsap.utils.random(-120,120),
        z: () => gsap.utils.random(-200,200),
    }, 
    {
        ease: 'none',
        opacity: 1,
        rotateX: 0,
        z: 0,
        delay: 10,
        stagger: 0.02,
        scrollTrigger: {
            trigger: title,
            start: 'top bottom',
            end: 'bottom top',
            scrub: true,
        }
    });

});
}



/**
 * Base
 */
// Debug
const debugObject = {}


// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Overlay
const overlayGeometry = new THREE.PlaneGeometry(2, 2, 1, 1)
const overlayMaterial = new THREE.ShaderMaterial({
  transparent: true,
  uniforms: {
    uAlpha: { value: 1 }
  },
  vertexShader: `
  void main()
  {
    gl_Position = vec4(position, 1.0);
  }
  `,
  fragmentShader: `
  uniform float uAlpha;
  void main()
  {
    gl_FragColor = vec4(0.05098, 0.04314, 0.04314, uAlpha);
  }
  `
})

const overlay =  new THREE.Mesh(overlayGeometry,overlayMaterial)
scene.add(overlay)



/**
 * Loaders
 */
// Texture loader
const textureLoader = new THREE.TextureLoader()

// Draco loader
const dracoLoader = new DRACOLoader()
dracoLoader.setDecoderPath('https://www.gstatic.com/draco/versioned/decoders/1.5.6/')

// GLTF loader
const loadingManager = new THREE.LoadingManager( 
  () => {
    gsap.to(loaderImage, {alpha: 0, duration: 3})
    gsap.to(overlayMaterial.uniforms.uAlpha, {duration: 4, delay: 3, value: 0})
    scroll()

  },
  () => {
    console.log('progress');
  }
)
const gltfLoader = new GLTFLoader(loadingManager)
gltfLoader.setDRACOLoader(dracoLoader)

/**
 * Object
 */

const bakedTexture = textureLoader.load('/static/baked.jpg')
bakedTexture.flipY = false
bakedTexture.encoding = THREE.sRGBEncoding

const bakedMaterial = new THREE.MeshBasicMaterial({map: bakedTexture})
const portalLightMaterial = new THREE.ShaderMaterial({ 
  uniforms: {
    uTime: {value: 0},
    uColorEnd: {value: new THREE.Color(0xd3d4d3)},
    uColorStart: {value: new THREE.Color(0.157, 0.161, 0.157)},
  },
  vertexShader: portalVertexShader,
  fragmentShader: portalFragmentShader,
})




gltfLoader.load(
  '/static/veil.glb',
  (gltf) => {
  
    const bakedMesh = gltf.scene.children.find((child) => child.name === 'baked')
    const portalLightMesh = gltf.scene.children.find((child) => child.name === 'Plane')

    bakedMesh.material = bakedMaterial;
    portalLightMesh.material = portalLightMaterial;
    const model = gltf.scene
    model.position.setY(-2.5);
    scene.add(model);
  }
)


// Geometry 

const fireFliesGeometry = new THREE.BufferGeometry();
const fireFliesCount = 30
const positionArray = new Float32Array(fireFliesCount * 3);
const scaleArray = new Float32Array(fireFliesCount)

for(let i = 0; i < fireFliesCount; i++) 
{
  positionArray[i * 3 + 0] = (Math.random() - 0.5) * 11;
  positionArray[i * 3 + 1] = Math.random() * 11;
  positionArray[i * 3 + 2] = (Math.random() - 0.5) * 11;

  scaleArray[i] = Math.random()
}


fireFliesGeometry.setAttribute('position', new THREE.BufferAttribute(positionArray, 3));
fireFliesGeometry.setAttribute('aScale', new THREE.BufferAttribute(scaleArray, 1));

// Material

const fireFliesMaterial = new THREE.ShaderMaterial({ 
  uniforms: {
    uTime: {value: 0},
    uPixelRatio: {value: Math.min(window.devicePixelRatio, 2)},
    uSize: {value: 400}
  },
  vertexShader: fireFliesVertexShader,
  fragmentShader: fireFliesFragmentShader,
  transparent: false,
  blending: THREE.AdditiveBlending,
  depthWrite: false,
});
const fireFlies = new THREE.Points(fireFliesGeometry, fireFliesMaterial);
scene.add(fireFlies)



/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

    // update fireflies
    fireFliesMaterial.uniforms.uPixelRatio.value = Math.min(window.devicePixelRatio, 2);
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(35, sizes.width / sizes.height, 0.5, 1000)
camera.position.x = 20
camera.position.y = 15
camera.position.z = 10
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

renderer.setClearColor('#0d0b0b')


/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()
    fireFliesMaterial.uniforms.uTime.value = elapsedTime
    portalLightMaterial.uniforms.uTime.value = elapsedTime

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()

